
-= Running CellX on a computer cluster with a queueing system =- 


Requirements for the cluster system are

- a shared filesystem for images and results
- a qsub program (provided e.g. by SGE or Torque/PBS)

On the head node:

- Linux with X server
- Perl with xml modules libxml-perl libxml-dom-perl libxml-regexp-perl
- java 1.6

On the compute nodes:

- Linux
- Matlab compiler runtime MCR 
  (note, this must be the MCR version contained in the CellX package)


1) Login to the cluster headnode with X display connection 
   (e.g., ssh -X headnode)

2) Copy the CellX linux files to the shared file system
   and install the MCR (required only once).

3) Adjust the following lines of submit.pl script in the cluster 
   subdirectory to point to the directories where CellX and the MCR 
   are installed (required only once). Please use absolute paths 
   that are valid on all nodes.

    my $cellxroot = "CELLX-HOME";
    my $mcr       = "MCR-HOME/v715";

4) Copy the images to the cluster file system 

5) Run 
   
   java -jar CellXGui.jar 
   
   and configure the image analysis as usual.

6) Save the parameters to a file config.xml (File -> Export Parameters)

8) Save the file series to a file series.xml (File -> Export File Series)

9) Run 

   ./submit.pl config.xml series.xml [INDEX-OF-SERIES]

10) When all jobs have finished and tracking was enabled, run

   ./runTracker.sh

11) The result files are placed into the folder selected in step 5).



