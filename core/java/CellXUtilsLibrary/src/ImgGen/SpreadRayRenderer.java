/*
 * SpreadRayRenderer.java
 *
 * Copyright (C) 2012 Christian Mayer
 *
 * This file is part of CellXUtilsLibrary
 *
 * CellXUtilsLibraryis free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * CellXUtilsLibrary is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package ImgGen;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GradientPaint;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.geom.GeneralPath;
import java.awt.geom.Point2D;

/**
 *
 * @author Christian Mayer <chrixian.mayer@gmail.com>
 */
public class SpreadRayRenderer {
    
    public static void render(
            Graphics2D g2, 
            Dimension d, 
            Color c1, 
            Color c2, 
            int y1, 
            int y2,
            double degrees,
            double width2){
        
        final double angle = degrees / 360.0 * 2 * Math.PI;
        final int n = (int) (360.0 / degrees);
        final AffineTransform a = g2.getTransform();

        final GeneralPath gp = new GeneralPath();
        gp.moveTo(0, 0);
        final double len = Math.max(d.height, d.width)+1;
        gp.lineTo(width2, len);
        gp.lineTo(-width2, len);
        gp.lineTo(0,0);
        
        for (int k = 0; k < n; ++k) {
            g2.setTransform(a);
            g2.rotate(k * angle);
            g2.setPaint(new GradientPaint(0, y1, c1, 0, y2, c2));
            g2.fill(gp);
        }
        g2.setTransform(a);
    }

    
}
