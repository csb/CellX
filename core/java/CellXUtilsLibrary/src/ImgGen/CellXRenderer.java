/*
 * CellXRenderer.java
 *
 * Copyright (C) 2012 Christian Mayer
 *
 * This file is part of CellXUtilsLibrary
 *
 * CellXUtilsLibraryis free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * CellXUtilsLibrary is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package ImgGen;

import java.awt.Color;
import java.awt.GradientPaint;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.Shape;
import java.awt.geom.AffineTransform;
import java.awt.geom.GeneralPath;
import java.awt.image.BufferedImage;

/**
 *
 * @author Christian Mayer <chrixian.mayer@gmail.com>
 */
public class CellXRenderer {

    public static void drawCircularRays(Graphics2D g2, int len, double degrees, Color c1, Color c2, int y1, int y2) {
        final double angle = degrees / 360.0 * 2 * Math.PI;
        final int n = (int) (360.0 / degrees);
        final AffineTransform a = g2.getTransform();
        for (int k = 0; k < n; ++k) {
            g2.setTransform(a);
            g2.rotate(k * angle);
            g2.setPaint(new GradientPaint(0, y1, c1, 0, y2, c2));
            g2.drawLine(0, 0, 0, len);
        }
        g2.setTransform(a);
    }

    public static Shape generateTip(double scale) {
        GeneralPath p = new GeneralPath();
        final double h1 = scale * 0.6;
        final double w1 = scale * 1.1;
        p.moveTo(0, 0);
        p.lineTo(0, h1);
        p.lineTo(w1, 0);
        p.lineTo(0, -h1);
        p.lineTo(0, 0);
        return p;
    }

    
    
     public static BufferedImage generateColorRayImage(int w, int h, double degrees) {
        final BufferedImage img = new BufferedImage(w, h, BufferedImage.TYPE_INT_RGB);
        final Graphics2D g2 = img.createGraphics();
        final int x = w / 2;
        final int y = h / 2;
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BICUBIC);
        g2.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
        g2.setRenderingHint(RenderingHints.KEY_STROKE_CONTROL, RenderingHints.VALUE_STROKE_NORMALIZE);

        final double angle = degrees / 360.0 * 2 * Math.PI;
        final int n = (int) (360.0 / degrees);
        System.out.println("Number of rays: "+n);

        g2.translate(x, y);

        float[] color = Color.RGBtoHSB(200, 60, 60, null);

        final float step = 2.0f / n;
        System.out.println("Color step = " + step);

        final AffineTransform ra = g2.getTransform();

        for (int k = 0; k < n; ++k) {
            g2.setTransform(ra);
            g2.rotate(k * angle);
            final int max = 300;
            g2.setPaint(new GradientPaint(0, 0, new Color(0, 0, 0, 255), 0, max, new Color(Color.HSBtoRGB(color[0], color[1], color[2])), true));
            g2.drawLine(0, 0, 0, w + h);
            color[0] = color[0] + step;
            if (color[0] > 1) {
                color[0] = color[0] - 1;
            }
        }
        return img;
    }

    
    
}
