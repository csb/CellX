/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ImgGen;

import java.awt.Shape;
import java.awt.geom.GeneralPath;
import java.util.ArrayList;
import java.util.List;
import math.geom2d.Point2D;

/**
 *
 * @author cmayer
 */
public class LooseEndBezierSpline {

    private final List<Point2D> knots = new ArrayList<Point2D>();

    public void addKnot(Point2D p) {
        knots.add(p);
    }

    public Shape generateSpline() {



        GeneralPath gp = new GeneralPath();


        gp.moveTo(knots.get(1).getX(), knots.get(1).getY());
        for(int i=0; i<knots.size()-3; ++i){
            
            final Point2D p1 = knots.get(i);
            final Point2D p2 = knots.get(i+1);
            final Point2D p3 = knots.get(i+2);
            final Point2D p4 = knots.get(i+3);
            
            final double[] controls = BezierSpline.computeControlPoints(
                    p1.getX(), p1.getY(), p2.getX(), p2.getY(), p3.getX(), p3.getY(), p4.getX(), p4.getY(), 1.0);
        
            
            gp.curveTo(controls[0], controls[1], controls[2], controls[3], p3.getX(), p3.getY());
            
        }
        
        
        
        
        return gp;

    }

    public Iterable<Point2D> getKnots() {
        return knots;
    }
}
