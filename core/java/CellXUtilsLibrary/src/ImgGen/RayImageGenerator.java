/*
 * RayImageGenerator.java
 *
 * Copyright (C) 2012 Christian Mayer
 *
 * This file is part of CellXUtilsLibrary
 *
 * CellXUtilsLibraryis free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * CellXUtilsLibrary is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package ImgGen;

import cellxgui.model.CellXModel;
import cellxgui.model.shapes.Coordinates;
import cellxgui.model.shapes.MembraneProfile;
import ij.ImagePlus;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.Shape;
import java.awt.geom.AffineTransform;
import java.io.File;
import java.io.IOException;
import javax.swing.JFrame;
import org.apache.batik.dom.svg.SVGDOMImplementation;
import org.apache.batik.svggen.SVGGraphics2D;
import org.apache.batik.swing.JSVGCanvas;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.svg.SVGDocument;

/**
 *
 * @author Christian Mayer <chrixian.mayer@gmail.com>
 */
public class RayImageGenerator {

    // public static double[] MEMBRANE_PROFILE = new double[]{-0.1204, -0.1330, -0.1482, -0.1932, -0.2918, -0.4302, -0.5332, -0.4356, -0.1201, 0.2417, 0.4403, 0.4668, 0.4104, 0.3156, 0.2181, 0.1399, 0.0864, 0.0478, 0.0233, 0.0153};
    //  public static int MEMBRANE_LOCATION=9; 
    public static double[] MEMBRANE_PROFILE = new double[]{-0.1191, -0.1528, -0.2362, -0.4203, -0.5613, -0.4113, -0.0417, 0.2682, 0.4387, 0.4032, 0.3396, 0.2228, 0.1469, 0.0796, 0.0435};
    public static int MEMBRANE_LOCATION = 7;
    public static Color PROFILE_COLOR = Color.red;
    public static Color INTENSITIES_COLOR = Color.yellow;
    public static Color CONOVLUTION_COLOR = Color.cyan;
    public static Color EXAMPLE_RAY_COLOR = Color.red;
    public static Color RAY_COLOR = Color.green;
    public static Color MINI_CHART_BACKGROUND = new Color(230, 230, 230);
    public static double relChartHeight = 0.3;
    public static int SEED_CROSS_LENGTH = 2;
    public static float GRAPH_THICKNESS = 1.2f;

    public static Dimension generateRayOverlayFigure(Graphics2D g2, String file, double degrees, int fadeIn, double[] profile, int offset, int x, int y) {

        final ImagePlus ip = CellXModel.loadImageAndConvertToGrayScale(new File(file));
        final int w = ip.getWidth();
        final int h = ip.getHeight();


        //DONT USE g2.clipRect or the resulting SVG cannot be converted to PDF properly!
        g2.setClip(0, 0, w, h);

        g2.drawImage(ip.getBufferedImage(), 0, 0, w, h, null);



        /*
         * draw circular rays at seed
         */
        //final int x = w / 2;
        //final int y = h / 2;
        final AffineTransform baseTransform = g2.getTransform();
        g2.translate(x, y);
        CellXRenderer.drawCircularRays(g2, Math.max(w, h), degrees, new Color(0, 0, 0, 0), RAY_COLOR, 0, fadeIn);



        /*
         * draw the seed cross and label
         */
        g2.setColor(RAY_COLOR);
        g2.setStroke(new BasicStroke(1.8f));
        g2.drawLine(-SEED_CROSS_LENGTH, 0, SEED_CROSS_LENGTH, 0);
        g2.drawLine(0, -SEED_CROSS_LENGTH, 0, SEED_CROSS_LENGTH);

        g2.setColor(Color.white);
        g2.setStroke(new BasicStroke(0.7f));
        g2.drawLine(-SEED_CROSS_LENGTH + 1, 0, SEED_CROSS_LENGTH - 1, 0);
        g2.drawLine(0, -SEED_CROSS_LENGTH + 1, 0, SEED_CROSS_LENGTH - 1);


        //g2.setColor(Color.white);
        //g2.drawString("S1", (int)(-g2.getFontMetrics().stringWidth("S1")*0.5), -(SEED_CROSS_LENGTH+3));



        /*
         * draw the intensity graph
         */
        final MembraneProfile mp = new MembraneProfile(new Coordinates(x, y, w, h / 2), Color.BLACK, Color.BLACK, ip.getProcessor());

        final Shape intensityShape = generateChartShape(mp.getNormalizedValues(), -relChartHeight * h, SEED_CROSS_LENGTH + 1);

        System.out.println();
        for (int i = 0; i < mp.getLength(); ++i) {
            System.out.print(mp.getNormalizedValues()[i] + " ");
        }
        System.out.println();
        g2.setColor(INTENSITIES_COLOR);
        g2.setStroke(new BasicStroke(
                GRAPH_THICKNESS,
                BasicStroke.CAP_BUTT,
                BasicStroke.JOIN_BEVEL));
        g2.draw(intensityShape);


        /*
         * draw the convolution graph
         */
        final double[] conv = Convolutor.normalize(Convolutor.convolute(mp.getNormalizedValues(), profile, offset));
        final Shape convShape = generateChartShape(conv, -relChartHeight * h, SEED_CROSS_LENGTH + 1);
        g2.setColor(CONOVLUTION_COLOR);
        g2.setStroke(new BasicStroke(GRAPH_THICKNESS));
        g2.draw(convShape);


        for (int i = 0; i < conv.length; ++i) {
            System.out.print(conv[i] + " ");
        }
        System.out.println();

        
        
        /*
         * draw the target ray
         */
        g2.setColor(EXAMPLE_RAY_COLOR);
       /* g2.setStroke(new BasicStroke(
                GRAPH_THICKNESS,
                BasicStroke.CAP_BUTT,
                BasicStroke.JOIN_MITER,
                1f,
                new float[]{1.618f, .618f},
                0f));*/
        g2.setStroke(new BasicStroke(
                GRAPH_THICKNESS,
                BasicStroke.CAP_BUTT,
                BasicStroke.JOIN_MITER,
                1f));
        g2.drawLine(SEED_CROSS_LENGTH + 1, 0, Math.max(w, h), 0);

        
        /*
         * draw the minichart
         */
        final int border = 5;
        g2.setTransform(baseTransform);
        g2.translate(w / 2, h / 2);
        final int insetWidth = MEMBRANE_PROFILE.length+2*border+1;
        final int insetHeight = insetWidth;

        drawMiniChart(g2, -(w / 2) + border, -(h / 2) + border + insetHeight, insetWidth, insetHeight, profile);

        return new Dimension(w, h);
    }

    public static Shape generateChartShape(double[] invals, double yScale, int start) {
        /* final GeneralPath p = new GeneralPath();
        
        final double[] values = new double[invals.length];
        for (int i = start; i < values.length; ++i) {
        values[i] = yScale * invals[i];
        }
        
        int i = start;
        p.moveTo(i, 0);
        for (; i < values.length - 2; i += 3) {
        p.curveTo(i, values[i], (i + 1), values[i + 1], (i + 2), values[i + 2]);
        }
        //return p;
         */
        return BezierSpline.createBezierSpline(transformToXY(invals, yScale, start), BezierSpline.EndMode.SMOOTH_START_AND_END, 1.0);

    }

    public static Shape generateChartShapeLineSeg(double[] values, double yScale, int start) {
        /* final GeneralPath p = new GeneralPath();
        for (int i = start; i < values.length; ++i) {
        values[i] = yScale * values[i];
        }
        
        int i = start;
        p.moveTo(i, values[0]);
        for (; i < values.length; ++i) {
        p.lineTo(i, values[i]);
        }
        return p;*/
        return BezierSpline.createBezierSpline(transformToXY(values, yScale, start), BezierSpline.EndMode.SMOOTH_START_AND_END, 0.3);
    }

    public static double[][] transformToXY(double[] values, double yScale, int start) {
        final int n = values.length;
        final double[][] ret = new double[n - start][];
        for (int i = start; i < n; ++i) {
            ret[i - start] = new double[]{i, values[i] * yScale};
        }
        return ret;
    }

    public static void drawMiniChart(Graphics2D g2, int x, int y, int w, int h, double[] values) {

        final int inset = 5;

        final AffineTransform a = g2.getTransform();

        double min = Double.MAX_VALUE;
        double max = Double.MIN_VALUE;
        for (int i = 0; i < values.length; ++i) {
            min = Math.min(min, values[i]);
            max = Math.max(max, values[i]);
        }

        final double range = Math.abs(max - min);

        g2.translate(x, y);
        g2.scale(1.0, -1.0);

        g2.setColor(MINI_CHART_BACKGROUND);
        g2.fillRect(0, 0, w, h);
        g2.setColor(Color.DARK_GRAY);
        g2.setStroke(new BasicStroke(0.4f));
        g2.drawRect(0, 0, w, h);

        g2.setColor(Color.DARK_GRAY);
        g2.setStroke(new BasicStroke(GRAPH_THICKNESS * 0.8f));
        g2.drawLine(inset, inset, inset, h - inset);
        final AffineTransform a3 = g2.getTransform();
        g2.translate(inset, h - inset);
        g2.rotate(Math.PI / 2);
        g2.fill(CellXRenderer.generateTip(2.5));
        g2.setTransform(a3);

        g2.translate(inset, (-min / range) * (h - 2 * inset) + inset + 1);

        g2.drawLine(0, 0, w - 2 * inset, 0);

        final AffineTransform a2 = g2.getTransform();
        g2.translate(w - 2 * inset, 0);
        g2.fill(CellXRenderer.generateTip(2.5));
        g2.setTransform(a2);

        g2.setStroke(new BasicStroke(1.2f, BasicStroke.CAP_BUTT, BasicStroke.JOIN_ROUND));
        g2.setColor(PROFILE_COLOR);
        final Shape s = generateChartShapeLineSeg(values, (h - 2 * inset) / range, 0);
        g2.translate(+1, 0);
        g2.draw(s);
        g2.setTransform(a);
    }

    public static void main(String[] args) {


        //plot("left_seed.svg", "./cell1.tif", 44, 78, MEMBRANE_PROFILE, 9);
        plot("seed1.svg", "./seed1_242x242.tiff", 121, 121, MEMBRANE_PROFILE, MEMBRANE_LOCATION);
        plot("seed2.svg", "./seed2_242x242.tiff", 121, 121, MEMBRANE_PROFILE, MEMBRANE_LOCATION);
        //plot("right_seed.svg", "./cell1.tif", 99, 73, MEMBRANE_PROFILE, 9);

        //plot("center_seed.svg", "./cell1.tif", 151/2, 151/2, MEMBRANE_PROFILE, 9);

    }

    public static void saveToSVGFile(SVGContainer svg, File f) {
        try {
            svg.getG2().stream(f.toString());
            System.out.println("Wrote " + f);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static SVGContainer createRayFigureSVGDocument(int x, int y, String image, double[] membraneProfile, int membraneOffset) {

        DOMImplementation impl = SVGDOMImplementation.getDOMImplementation();
        String svgNS = SVGDOMImplementation.SVG_NAMESPACE_URI;
        SVGDocument doc = (SVGDocument) impl.createDocument(svgNS, "svg", null);

        SVGGraphics2D g2 = new SVGGraphics2D(doc);

        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BICUBIC);
        g2.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
        g2.setRenderingHint(RenderingHints.KEY_STROKE_CONTROL, RenderingHints.VALUE_STROKE_NORMALIZE);


        Dimension d = ImgGen.RayImageGenerator.generateRayOverlayFigure(g2, image, 10, 30, membraneProfile, membraneOffset, x, y);
        g2.setSVGCanvasSize(d);

        return new SVGContainer(g2, doc);
    }

    private static void plot(String resultfile, String image, int x, int y, double[] membraneProfile, int membraneOffset) {
        SVGContainer svg = createRayFigureSVGDocument(x, y, image, membraneProfile, membraneOffset);
        JSVGCanvas canvas = new JSVGCanvas();

        svg.getG2().getRoot(svg.getDoc().getRootElement());
        canvas.setSVGDocument(svg.getDoc());
        canvas.setEnableImageZoomInteractor(true);
        canvas.setEnableZoomInteractor(true);

        JFrame frame = new JFrame();
        frame.getContentPane().add(canvas);
        frame.pack();
        frame.setSize(600, 600);

        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

        svg = createRayFigureSVGDocument(x, y, image, membraneProfile, membraneOffset);

        saveToSVGFile(svg, new File(resultfile));
    }
}
