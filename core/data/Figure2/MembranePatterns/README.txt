%% MEMBRAVE PATTERNS DESCRIPTION

% case of Membrane Pattern Bright Field
% single cell , below focal plane
if ccase==311
    
    settings = getSettings();
    settings.UseInit = 1;
    settings.UseAdaptForGC = 1 ;
    settings.FigureNumber = 2;
    settings.debug = 1;
    settings.radMat = [7 15];
    settings.ElongationLength = 50;
    settings.CropInteract = 1;
    settings.CropRegion = [ 195 66 100 100];
    settings.IsNotConvex = 1;
    settings.ExpansionThresh = 0.5; 
    settings.MembraneSignal = [   0.6268    0.5290    0.3544    0.2718    0.1584    0.1662, ...
                                  0.3360    0.3360    0.4740    0.5468    0.6500 ];
    settings.MembraneSignalLocation = 6;    
    settings.PercOfSuccRayThreshold = 0.85;
    settings.MembraneWidth = 4;
    settings.SeedSensitivity  = 0.4; 
    
    settings.bf_out_of_focus_image = [loc 'Figure3_crowded/Shapes/AbberantYeastCells/pos1/BF_position040511_time0001.tif'];
    settings.bf_in_focus_image     = [loc 'Figure3_crowded/Shapes/AbberantYeastCells/pos1/BF_position040516_time0001.tif'];
    settings.seg_image_out = '../ResultFolder/seg_test_shapes.png';
    settings.results_file  = '../ResultFolder/results_test_shapes.txt';
    settings.frameno =  1;

   checkSettings(settings);
   main(settings);
end


% case of Membrane Pattern Phase Contrast 
% single cell
if ccase==312
    settings = getSettings();
    settings.UseInit = 1;
    settings.UseAdaptForGC = 0 ;
    settings.NonConstantPattern = 0 ;
    settings.FigureNumber =21;
    settings.debug =1;
    settings.radMat = [25 35];
    settings.ElongationLength = 120;
    settings.CropInteract = 1;
    settings.CropRegion = [1115 470  190  190];
    settings.IsNotConvex = 0;
    settings.ExpansionThresh = 1; 
    settings.MembraneSignal = [0.4, 0.45 , 0.55 ,0.65 , 0.7, 0.75, 0.8, ones(1,5),...
                                0.95, 0.85 , 0.8 , 0.7 ,0.65, 0.55 ];
    settings.MembraneSignalLocation = 8;
    settings.MembraneWidth = 6;
    settings.SeedSensitivity  = 0.3; 
    
    settings.bf_out_of_focus_image = [loc 'Figure3_crowded/MembranePattern/PhaseContrast/phase_04.tif'];
    settings.bf_in_focus_image     = [loc 'Figure3_crowded/MembranePattern/PhaseContrast/phase_04.tif'];
    settings.seg_image_out = '../ResultFolder/seg_test_mpattern_phcon.png';
    settings.results_file  = '../ResultFolder/results_mpattern_phcon.txt';
    settings.frameno =  1;

   checkSettings(settings);
   main(settings);
end


% case of Membrane Pattern - fluorescence
% membrane stain  
if ccase==313
    settings = getSettings();
    settings.UseInit = 1;
    settings.UseAdaptForGC = 1 ;
    settings.NonConstantPattern = 0 ;
    settings.FigureNumber = 2;
    settings.debug =1;
    settings.radMat = [20 30];
    settings.ElongationLength = 60;
    %settings.CropInteract = 1;
    settings.CropRegion = [ 865 177 158 151];
    %settings.IsNotConvex = 0;
    settings.ExpansionThresh = 0.5;   
    settings.MembraneSignal =  [ 0.4810    0.4912    0.5385    0.7256    0.7680    0.9200,...
                                 0.8662    0.7378    0.5148    0.4828    0.4650];
    settings.MembraneSignalLocation = 7;    
    settings.PercOfSuccRayThreshold = 0.8;
    settings.MembraneWidth = 5;
    settings.SeedSensitivity  = 0.3; 
    
    settings.bf_out_of_focus_image = [loc 'Figure3_crowded/MembranePattern/fluorescence/MembraneStain/pos1/ph2_position010100_time0001.tif'];
    settings.bf_in_focus_image     = [loc 'Figure3_crowded/MembranePattern/fluorescence/MembraneStain/pos1/BFout_position010100_time0001.tif'];
    settings.seg_image_out = '../ResultFolder/seg_test_ph.png';
    settings.results_file  = '../ResultFolder/results_test_ph.txt';
    settings.frameno =  1;

   checkSettings(settings);
   main(settings);
end

% case of Membrane Pattern - fluorescence
% cytosolic 
if ccase==314
    settings = getSettings();
    settings.UseInit = 1;
    settings.NonConstantPattern = 0 ;
    settings.IsNotConvex  = 0;
    settings.UseAdaptForGC = 0 ;
    settings.FigureNumber = 2;
    settings.debug =1;
    settings.radMat = [17 31];
    settings.ElongationLength = 60;
    %settings.CropInteract = 1;
    settings.CropRegion = [ 810 1007 102  102];
    settings.IsNotConvex = 0;
    settings.ExpansionThresh = 0.5;   
    settings.MembraneSignal =  [   0.6056    0.5702    0.5138    0.4582    0.4259    0.4094,...
                                   0.3987    0.3922    0.3922    0.3809    0.3569    0.2823  0.2476    0.2038    0.1981  ];
    settings.MembraneSignalLocation = 8;    
    settings.PercOfSuccRayThreshold = 0.85;
    settings.MembraneWidth = 3;
    settings.SeedSensitivity  = 0.1; 
    
    settings.bf_out_of_focus_image = [loc 'Figure3_crowded/MembranePattern/fluorescence/Cytoplasmic/ph/ph2_position010100_time0001.tif'];
    settings.bf_in_focus_image     = [loc 'Figure3_crowded/MembranePattern/fluorescence/Cytoplasmic/ph/BF_position010100_time0001.tif'];
    settings.seg_image_out = '../ResultFolder/seg_test_ph.png';
    settings.results_file  = '../ResultFolder/results_test_ph.txt';
    settings.frameno =  1;

   checkSettings(settings);
   main(settings);
end


if ccase==315
    settings = getSettings();
    settings.UseInit = 1;
    settings.UseAdaptForGC = 0 ;
    settings.NonConstantPattern = 0 ;
    settings.FigureNumber = 21;
    settings.debug =1;
    settings.radMat = [7 17];
    settings.ElongationLength = 50;
    settings.CropInteract = 1;
    settings.CropRegion =   [149 109 75  75];

    settings.IsNotConvex = 0;
    settings.ExpansionThresh = 0.5;   
    settings.MembraneSignal = [ 0.0387    0.0760    0.0837    0.1726    0.1961    0.3173,...
                                0.3592    0.5320    0.5708    0.6336    0.6343    0.6715];
    settings.MembraneSignalLocation = 7;    
    settings.PercOfSuccRayThreshold    = 0.8;
    settings.MembraneWidth = 3;
    settings.SeedSensitivity  = 0.2; 
    
    settings.bf_out_of_focus_image = [loc 'Figure3_crowded/MembranePattern/fluorescence/Exterior/RFP_position010100_time0039.tif'];
    settings.bf_in_focus_image     = [loc 'Figure3_crowded/MembranePattern/fluorescence/Exterior/BF_position010100_time0039.tif'];
    settings.seg_image_out = '../ResultFolder/seg_test_ph.png';
    settings.results_file  = '../ResultFolder/results_test_ph.txt';
    settings.frameno =  1;

   checkSettings(settings);
   main(settings);

end

