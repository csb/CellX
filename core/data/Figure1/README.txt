    settings = getSettings();
    settings.UseInit  = 1;
    settings.UseAdaptForGC= 0;
    settings.FigureNumber =1;
    settings.debug =0;
    settings.radMat = [17 27];
    settings.ElongationLength = 100;
    settings.CropInteract = 0;
    settings.CropRegion = [1100 150 150 150];
    settings.IsNotConvex = 0;
    settings.ExpansionThresh = 1; 
    settings.MembraneSignal = [0.3,0.2,0.1,0.1,0.16,0.37,0.56,0.72,0.90,0.90,0.78,0.6,0.5];
    settings.MembraneSignalLocation = 5;
    settings.MembraneWidth = 5;
    settings.SeedSensitivity  = 0.25; 
    
    settings.bf_out_of_focus_image = [loc 'Figure2_pombe/Trans_02_tiff/Trans_020011.tif'];
    settings.bf_in_focus_image     = [loc 'Figure2_pombe/Trans_02_tiff/Trans_020007.tif'];
    settings.seg_image_out = '../ResultFolder/seg_test_pombe.png';
    settings.results_file  = '../ResultFolder/results_test_pombe.txt';
    settings.frameno =  1;

   checkSettings(settings);
   main(settings);
