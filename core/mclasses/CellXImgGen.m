classdef CellXImgGen
    %CELLXIMAGEGENERATOR Summary of this class goes here
    %   Detailed explanation goes here
    
    
    
    methods (Static)
        
        function rgb = generateSegmenterDebugImage(seeds, baseImg)
            rgb = CellXImgGen.generateHoughTransformImage(seeds, baseImg);
            r = rgb(:,:,1);
            g = rgb(:,:,2);
            b = rgb(:,:,3);
            seedCount = size(seeds,2);
            
            for i = 1:seedCount
                s = seeds(i);
                ind = s.perimeterPixelListLindx;
                
                if(s.skipped == 0)
                    r(ind) = 0.4+0.6*rand(1);
                    g(ind) = 0.4+0.6*rand(1);
                    b(ind) = 0.4+0.6*rand(1);
                elseif( s.skipped>2 )
                    dashedInd = ind(1:3:end);
                    rc = 0.4+0.6*rand(1);
                    gc = 0.4+0.6*rand(1);
                    bc = 0.4+0.6*rand(1);
                    r(dashedInd) = rc;
                    g(dashedInd) = gc;
                    b(dashedInd) = bc;
                end
                
            end
            rgb = cat(3, r, g, b);
            
            %            %imwrite(rgb, [filename '_raw.png'], 'png');
            %
            %            dim = size(this.image);
            %            h = figure;
            %            set(h, 'Position', [0 0 dim(1) dim(2)]);
            %            image(rgb);
            %            axis image;
            %            axis off;
            %
            %            for i = 1:seedCount
            %                s = seeds(i);
            %                text(s.houghCenterX, s.houghCenterY, ['\fontsize{8}{\color{black}' num2str(i) '}' ],...
            %                    'HorizontalAlignment','center', ...
            %                    'BackgroundColor',[1 1 1],'EdgeColor','black');
            %            end
            % %           print(h, '-dpng', '-r300', filename);
            % %           close(h);
            
        end
        
        function [rgb, nucMask]= generateHoughTransformImageComparison(seeds, baseImg)
            r = baseImg;
            g = baseImg;
            b = baseImg;
            nucMask = zeros(size(b));
            if size(seeds,1)~=0;
            seedCount = size(seeds,2);
            for i = 1:seedCount
                s = seeds(i);
                seedX = s.houghCenterX;
                seedY = s.houghCenterY;
                radius = s.houghRadius;
                if(s.skipped==0)
                    start = max(1,seedX-radius);
                    e = min(size(baseImg,2), seedX+radius);
                    width = [seedY-1 seedY seedY+1];
                    for pix=start:1:e
                        r(width, pix) = 1;
                        g(width, pix) = 0;
                        b(width, pix) = 0;
                    end
                    start = max(1,seedY-radius);
                    e = min(size(baseImg,1), seedY+radius);
                    width = [seedX-1 seedX seedX+1];
                    for pix=start:1:e
                        r(pix, width) = 1;
                        g(pix, width) = 0;
                        b(pix, width) = 0;
                    end
                else
                    start = max(1,seedX-radius);
                    e = min(size(baseImg,2), seedX+radius);
                    for pix=start:2:e
                        r(seedY, pix) = 1;
                        g(seedY, pix) = 0;
                        b(seedY, pix) = 0;
                    end
                    start = max(1,seedY-radius);
                    e = min(size(baseImg,1), seedY+radius);
                    for pix=start:2:e
                        r(pix, seedX) = 1;
                        g(pix, seedX) = 0;
                        b(pix, seedX) = 0;
                    end
                end
                %find the pixel list of a circle with 
                % center (seedX ,seedY) and radius (radius/2)
                % grid size
                x = 1:size(b,2);
                y = 1:size(b,1);
                % make the grid
                [X,Y] = meshgrid(x,y);
                % define nuclear center
                c=[seedX ,seedY];
                
                % create nuclear circle:left side of cell in x axis
                Rnuc = radius/3;
                nuccir = (X-c(1)).^2 + (Y-c(2)).^2  <= Rnuc^2 ;
                %generate nuclear mask
                cnucmask =double(nuccir);                
                nucMask = nucMask + cnucmask;
                b(cnucmask>0)=1;
                
            end
            nucMask = nucMask>0;
            
            end
            rgb = cat(3,r,g,b);
        end
        
        
        
        function rgb = generateHoughTransformImage(seeds, baseImg)
            r = baseImg;
            g = baseImg;
            b = baseImg;
            seedCount = size(seeds,2);
            for i = 1:seedCount
                s = seeds(i);
                seedX = s.houghCenterX;
                seedY = s.houghCenterY;
                radius = s.houghRadius;
                if(s.skipped==0)
                    start = max(1,seedX-radius);
                    e = min(size(baseImg,2), seedX+radius);
                    width = [seedY-1 seedY seedY+1];
                    for pix=start:1:e
                        r(width, pix) = 1;
                        g(width, pix) = 0;
                        b(width, pix) = 0;
                    end
                    start = max(1,seedY-radius);
                    e = min(size(baseImg,1), seedY+radius);
                    width = [seedX-1 seedX seedX+1];
                    for pix=start:1:e
                        r(pix, width) = 1;
                        g(pix, width) = 0;
                        b(pix, width) = 0;
                    end
                else
                    start = max(1,seedX-radius);
                    e = min(size(baseImg,2), seedX+radius);
                    width = [seedY-1 seedY seedY+1];
                    for pix=start:2:e
                        r(width, pix) = 1;
                        g(width, pix) = 1;
                        b(width, pix) = 0;
                    end
                    start = max(1,seedY-radius);
                    e = min(size(baseImg,1), seedY+radius);
                    width = [seedX-1 seedX seedX+1];
                    for pix=start:2:e
                        r(pix, width) = 1;
                        g(pix, width) = 1;
                        b(pix, width) = 0;
                    end
                end
            end
            rgb = cat(3,r,g,b);
        end
        
        
        function rgb = generateControlImage(seeds, baseImg)
            
            r = baseImg;
            g = baseImg;
            b = baseImg;
            seedCount = size(seeds,2);
            for i = 1:seedCount
                s = seeds(i);
                seedX = s.houghCenterX;
                seedY = s.houghCenterY;
                radius = 1;
                
                cr = 0.4+0.6*rand(1);
                cg = 0.4+0.6*rand(1);
                cb = 0.4+0.6*rand(1);
                
                if(s.skipped==0)
                    start = max(1,seedX-radius);
                    e = min(size(baseImg,2), seedX+radius);
                    for pix=start:1:e
                        r(seedY, pix) = cr;
                        g(seedY, pix) = cg;
                        b(seedY, pix) = cb;
                    end
                    start = max(1,seedY-radius);
                    e = min(size(baseImg,1), seedY+radius);
                    for pix=start:1:e
                        r(pix, seedX) = cr;
                        g(pix, seedX) = cg;
                        b(pix, seedX) = cb;
                    end
                end
                ind = s.perimeterPixelListLindx;
                if(s.skipped == 0)
                    r(ind) = cr;
                    g(ind) = cg;
                    b(ind) = cb;
                end
            end
            rgb = cat(3, r, g, b);
        end
        
        
        
        
        %/ APC Patch: Faster implementation of control image generation 
        % with cell indices
        function createControlImageWithCellIndices(seeds, baseImg, varargin)
            
            r = baseImg;
            g = baseImg;
            b = baseImg;
            cr = 0.4+0.6*rand(1);
            cg = 0.4+0.6*rand(1);
            cb = 0.4+0.6*rand(1);
            crr = repmat(cr,numel(seeds(1).perimeterPixelListLindx),1);
            cgg = repmat(cg,numel(seeds(1).perimeterPixelListLindx),1);
            cbb = repmat(cb,numel(seeds(1).perimeterPixelListLindx),1);
            c = [seeds(1).perimeterPixelListLindx, crr, cgg, cbb];
            for i = 1:numel(seeds)
                cr = 0.4+0.6*rand(1);
                cg = 0.4+0.6*rand(1);
                cb = 0.4+0.6*rand(1);
                crr = repmat(cr,numel(seeds(i).perimeterPixelListLindx),1);
                cgg = repmat(cg,numel(seeds(i).perimeterPixelListLindx),1);
                cbb = repmat(cb,numel(seeds(i).perimeterPixelListLindx),1);
                c = [c; seeds(i).perimeterPixelListLindx, crr, cgg, cbb];
            end
            r(c(:,1)) = c(:,2);
            g(c(:,1)) = c(:,3);
            b(c(:,1)) = c(:,4);
            rgb = cat(3, r, g, b);
            
            fs = round(mean(vertcat(seeds.minorAxisLength)*0.5));
            if (fs < 10)
                fs = 10;
            end

            text_str = cell(numel(seeds),1);
            for ii=1:numel(seeds)
                text_str{ii} = num2str(ii);
            end

            Idx = [];
            for i = 1:numel(text_str)
                Idx = get_TextIndicesOnImage(rgb,text_str{i},[[seeds(i).houghCenterY]', [seeds(i).houghCenterX]'],'Arial',fs, Idx);
            end
            rgb(Idx+(size(rgb,1)*size(rgb,2))) = 1;
            
            if( size(varargin,2)==0 )
                offScreenFig = figure;
                imshow(rgb, 'Border', 'tight', 'InitialMagnification', 'fit');
                truesize;
                set(offScreenFig,'visible','on');
            elseif(size(varargin,2)==1)
                
                [x, map] = rgb2ind(rgb, 256);
                imwrite(x,map,varargin{1});
                
                fprintf('Wrote %s\n', varargin{1});
                
            else
                error('Too many variable arguments');
            end
        end
        %\ End APC Patch
        
        

         function color = getRandColor()       
            color = [rand(1) rand(1) rand(1)];
            percept = [0.2126; 0.7152; 0.0722];          
            v = var(color); 
            lum = color*percept;        
            k=0;          
            while( (lum<0.5 || v<0.04) &&k<10 )
                 color = [rand(1) rand(1) rand(1)];
                 v = var(color); 
                 lum = color*percept;
                 k = k+1;               
            end       
         end
        
        function rgb = generateIntensityExtractionControlImage(seeds, baseImg)
            
            %transparency of the membrane color
            alpha = 0.5;
            
            r = baseImg;
            g = baseImg;
            b = baseImg;
            seedCount = size(seeds,2);
            for i = 1:seedCount
                s = seeds(i);
                seedX = s.houghCenterX;
                seedY = s.houghCenterY;
                radius = 1;
                
                color = CellXImgGen.getRandColor();
                
                cr = color(1);
                cg = color(2);
                cb = color(3);
                
                if(s.skipped==0)
                    start = max(1,seedX-radius);
                    e = min(size(baseImg,2), seedX+radius);
                    for pix=start:1:e
                        r(seedY, pix) = cr;
                        g(seedY, pix) = cg;
                        b(seedY, pix) = cb;
                    end
                    start = max(1,seedY-radius);
                    e = min(size(baseImg,1), seedY+radius);
                    for pix=start:1:e
                        r(pix, seedX) = cr;
                        g(pix, seedX) = cg;
                        b(pix, seedX) = cb;
                    end
                end
                ind = s.membranePixelListLindx;
                if(s.skipped == 0)
                    r(ind) = min(1, alpha*r(ind) + (1-alpha)*cr);
                    g(ind) = min(1, alpha*g(ind) + (1-alpha)*cg);
                    b(ind) = min(1, alpha*b(ind) + (1-alpha)*cb);
                end
            end
            rgb = cat(3, r, g, b);
        end
        
        
        function rgb = generateIntensityExtractionControlBoundaryImage(seeds, baseImg)
            
            %transparency of the membrane color
            alpha = 0.5;
            
            r = baseImg;
            g = baseImg;
            b = baseImg;
            seedCount = size(seeds,2);
            for i = 1:seedCount
                s = seeds(i);
                seedX = s.houghCenterX;
                seedY = s.houghCenterY;
                radius = 1;
                
                color = CellXImgGen.getRandColor();
                
                cr = color(1);
                cg = color(2);
                cb = color(3);
                
                if(s.skipped==0)
                    start = max(1,seedX-radius);
                    e = min(size(baseImg,2), seedX+radius);
                    for pix=start:1:e
                        r(seedY, pix) = cr;
                        g(seedY, pix) = cg;
                        b(seedY, pix) = cb;
                    end
                    start = max(1,seedY-radius);
                    e = min(size(baseImg,1), seedY+radius);
                    for pix=start:1:e
                        r(pix, seedX) = cr;
                        g(pix, seedX) = cg;
                        b(pix, seedX) = cb;
                    end
                end
                ind = s.perimeterPixelListLindx;
                if(s.skipped == 0)
                    r(ind) = min(1, alpha*r(ind) + (1-alpha)*cr);
                    g(ind) = min(1, alpha*g(ind) + (1-alpha)*cg);
                    b(ind) = min(1, alpha*b(ind) + (1-alpha)*cb);
                end
            end
            rgb = cat(3, r, g, b);
        end        
        
    end
    
end
