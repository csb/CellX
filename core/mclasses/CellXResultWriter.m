classdef CellXResultWriter < handle
    %CELLXRESULTWRITER
    
    properties (Constant)
        
        segHeader = { ...
            'cell.frame', ...
            'cell.index', ...
            'cell.center.x', ...
            'cell.center.y', ...
            'cell.majoraxis', ...
            'cell.minoraxis', ...
            'cell.orientation', ...
            'cell.area', ...
            'cell.volume', ...
            'cell.perimeter', ...
            'cell.eccentricity', ...
            'cell.fractionOfGoodMembranePixels', ...
            'cell.mem.area', ...
            'cell.mem.volume', ...
            'cell.nuc.radius', ...
            'cell.nuc.area'};
        
        segFormat = '%d\t%d\t%d\t%d\t%d\t%d\t%+5.3f\t%d\t%d\t%d\t%1.5f\t%1.5f\t%d\t%d\t%d\t%d';
        
        fluoSuffixesHeader = {...
            '.background.mean',...
            '.background.std',...
            '.cell.total',...
            '.cell.qXX',...
            '.mem.total',...
            '.mem.qXX',...
            '.nuc.total',...
            '.nuc.qXX',...
            '.bright.total',...
            '.bright.qXX',...
            '.bright.euler'};
        
        
%         trackHeader = {...
%             'track.index',...          % the index of this track
%             'track.spanned',...        % 1 if the track spans this frame because the cell was not detected, 0 otherwise
%             'track.child.of.cell',...  % the segmentation index of the mother cell on the previous frame, 0 if there is no such cell
%             'track.child.of.track',... % the index of the path of the mother cell on the previous frame, 0 if there is no such track
%             'track.absorbs.cell',...  % the index of the track on the previous frame that is absorbed into this track, 0 if there is no such track
%             'track.absorbs.track',...   % the segmentation index of the cell on the previous frame that is absorbed into this track, 0 if there is no such cell
%             };
%         
%         trackFormat = '%d\t%d\t%d\t%d\t%d\t%d';
        
        
    end
    
    
    
    
    
    
    
    
    methods (Static)
        % writes an R compatible header line and table with
        % 14 fields per line for each seed/cell
        function writeTxtSegmentationResults(fSet, seeds, config)
            % first check how many fluo types we have and append
            % their tags into the fluoSuffixesHeader
            % extent also the fluoFormat
            nrOfFluoTags = numel(fSet.fluoTags);

            nFluoFeatures = 0;
            for fp = 1:numel(CellXResultWriter.fluoSuffixesHeader)
                currentFluoHeaderElement = CellXResultWriter.fluoSuffixesHeader{fp};

                if length(currentFluoHeaderElement) >= 3 && strcmp(currentFluoHeaderElement(end-2:end), 'qXX')
                    nFluoFeatures = nFluoFeatures + length(config.fluoQuantiles);
                else
                    nFluoFeatures = nFluoFeatures + 1;
                end
            end
            
            singleFluoFormat = strjoin(repmat({'%d'},1,nFluoFeatures), '\t');
            jointFluoHeader = [];
            jointFluoFormat = [];
            for ft=1:nrOfFluoTags
                % take the current fluoTag
                currentFluoTag = fSet.fluoTags{ft};
                % build the new fluoHeader
                currentFluoHeader = cell(1,0);
                for fp = 1:numel(CellXResultWriter.fluoSuffixesHeader)
                    currentFluoHeaderElement = CellXResultWriter.fluoSuffixesHeader{fp};
                    
                    if length(currentFluoHeaderElement) >= 3 && strcmp(currentFluoHeaderElement(end-2:end), 'qXX')
                        for qi = config.fluoQuantiles
                            currentFluoHeader{end + 1} = [currentFluoTag, currentFluoHeaderElement(1:end-2), sprintf('%02g', qi)];
                        end
                    else
                        currentFluoHeader{end + 1} = [currentFluoTag, currentFluoHeaderElement];
                    end
                end
                jointFluoHeader = [jointFluoHeader,currentFluoHeader];
                jointFluoFormat = [jointFluoFormat,'\t',singleFluoFormat];
            end
            % build the joint header
            jointHeader = [CellXResultWriter.segHeader,jointFluoHeader];
            % build the joint format
            jointFormat = [CellXResultWriter.segFormat,jointFluoFormat];
            
            % open the file
            out = fopen( fSet.seedsTxtFile , 'w');
            % write the headers
            
            fprintf(out, [strjoin(jointHeader,'\t') '\n']);
            
            

            n = length(seeds);
            for i = 1:n
                s = seeds(i);

                segCols =[...
                    fSet.frameIdx, ...
                    i, ...
                    round(s.centroid(1)), ...
                    round(s.centroid(2)), ...
                    round(s.majorAxisLength), ...
                    round(s.minorAxisLength), ...
                    s.orientation, ...
                    round(s.getCellArea()), ...
                    round(s.cellVolume), ...
                    round(s.perimeter), ...
                    s.eccentricity, ...
                    s.fracOfGoodMemPixels, ...
                    round(s.getMembraneArea()), ...
                    round(s.cellVolume - s.cytosolVolume), ...
                    round(s.getNucleusRadius(config)), ...
                    round(s.getNucleusArea(config))];
                
                fluoCols = CellXResultWriter.generateFluoCols(s, fSet, config);
                
                cellRow = [segCols,fluoCols];
                
                if i<n
                    fprintf(out, [jointFormat '\n'] ,cellRow);
                else
                    fprintf(out, jointFormat, cellRow);
                end
            end
            
            fclose(out);
        end
        
        
        
        
        function fluoCols = generateFluoCols(s, fSet, config)
            nrOfFluoTags = numel(fSet.fluoTags);
            
            nFluoFeatures = 7 + 4 * length(config.fluoQuantiles);
            % LW PATCH START
            %fluoCols = [];
            
            fluoCols = zeros(1, nFluoFeatures*nrOfFluoTags);
            % LW PATCH END
            for nrf=1:nrOfFluoTags
                
                fluoFeature = s.fluoFeatureList(nrf);
                
                currentFeatures = round([fluoFeature.backgroundMeanValue, ...
                    fluoFeature.backgroundStdValue, ...
                    fluoFeature.totalCellIntensity, ...
                    fluoFeature.totalCellIntensityQuantiles, ...
                    fluoFeature.totalMembraneIntensity, ...
                    fluoFeature.totalMembraneIntensityQuantiles, ...
                    fluoFeature.totalNuclearIntensity, ...
                    fluoFeature.totalNuclearIntensityQuantiles, ...
                    fluoFeature.totalBrightAreaIntensity, ...
                    fluoFeature.totalBrightAreaIntensityQuantiles, ...
                    fluoFeature.eulerNumberOfBrightArea]);
                
                
                fluoCols((1+(nrf-1)*nFluoFeatures):(nFluoFeatures+(nrf-1)*nFluoFeatures)) = currentFeatures;
            end
        end
        
        function writeMatSegmentationResults(filename, cells)
            w = warning ('off','all');
            cellStruct = []; 
            for i = 1:length(cells)
                cellStruct = [cellStruct struct(cells(i))];
            end
            warning(w);
            save(filename, 'cellStruct');
            fprintf('Wrote %s\n', filename);
            
        end
        
        
        % saves the recovered cells that were created by the tracker
        % recoveredCells is a structure with fields:
        %   seeds, an array of seeds
        %   frame, an array that holds the frame of seeds(i) at position i
        %   index, an array that holds the index of the cell wrt the other
        %          cells on that frame
        %
        %   tracks, a matrix with the track info
        %   tracksHeader, the column names of the tracks matrix
        function writeMatTrackerData(filename, trackerData)
            save(filename, 'trackerData');
            fprintf('Wrote %s\n', filename);
        end
        
        

        
        
        
        
        
        % Creates an image with the input image dimension
        % All valid cell pixels are set to the segmentation index of the
        % cell (>=1)
        % other pixels are 0
        function writeSegmentationMask(filename, cells, dim)
            segmentationMask = zeros(dim);
            for nrc = 1:length(cells)
                segmentationMask(cells(nrc).cellPixelListLindx) = nrc;
            end
            save(filename,'segmentationMask');
            fprintf('Wrote %s\n', filename);
        end
        
        
        
       function writeInitialInputImage(srcImgFile, dstImgFile, seeds, config)
            img = CellXImageIO.loadToGrayScaleImage(srcImgFile, config.cropRegionBoundary);
            img = CellXImageIO.normalize( img );
            %img = CellXImgGen.generateHoughTransformImage(seeds, img);
            imwrite(img, dstImgFile, 'png');
            fprintf('Wrote %s\n', dstImgFile);
        end
        
        
        function writeHoughTransformControlImage(srcImgFile, dstImgFile, seeds, config)
            img = CellXImageIO.loadToGrayScaleImage(srcImgFile, config.cropRegionBoundary);
            img = CellXImageIO.normalize( img );
            img = CellXImgGen.generateHoughTransformImage(seeds, img);
            imwrite(img, dstImgFile, 'png');
            fprintf('Wrote %s\n', dstImgFile);
        end
        
        function writeSegmentationControlImage(srcImgFile, dstImgFile, seeds, config)
            img = CellXImageIO.loadToGrayScaleImage(srcImgFile, config.cropRegionBoundary);
            img = CellXImageIO.normalize( img );
            img = CellXImgGen.generateControlImage(seeds, img);
            imwrite(img, dstImgFile, 'png');
            fprintf('Wrote %s\n', dstImgFile);
        end
        
        function writeSegmentationControlImageWithIndices(srcImgFile, dstImgFile, seeds, config)
            img = CellXImageIO.loadToGrayScaleImage(srcImgFile, config.cropRegionBoundary);
            img = CellXImageIO.normalize( img );
            CellXImgGen.createControlImageWithCellIndices(seeds, img,  dstImgFile);
        end
        
        function writeSeedingControlImage(srcImgFile, dstImgFile, seeds, config)
            img = CellXImageIO.loadToGrayScaleImage(srcImgFile, config.cropRegionBoundary);
            img = CellXImageIO.normalize( img );
            img = CellXImgGen.generateHoughTransformImage(seeds, img);
            [x, map] = rgb2ind(img, 256);
            %imwrite(img, dstImgFile, 'png'); LW
            imwrite(x, map, dstImgFile, 'png'); 
            fprintf('Wrote %s\n', dstImgFile);
        end
        
        function writeSegmentationControlImageWithIndicesB(srcImgFile, dstImgFile, seeds, config)
            img = CellXImageIO.loadToGrayScaleImage(srcImgFile, config.cropRegionBoundary);
            img = CellXImageIO.normalize( img );
            CellXImgGen.createControlImageWithCellIndicesB(seeds, img,  dstImgFile);
        end
        
        
        function writeFluoControlImage(srcImgFile, dstImgFile, seeds, config)
            img = CellXImageIO.loadToGrayScaleImage(srcImgFile, config.cropRegionBoundary);
            img = CellXImageIO.normalize( img );
            img = CellXImgGen.generateIntensityExtractionControlImage( ...
                seeds, ...
                img ...
                );
            imwrite(img, dstImgFile, 'png');
            fprintf('Wrote %s\n', dstImgFile);
        end
    end 
end

