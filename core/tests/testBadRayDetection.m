clear; clc; close;


thisFile = mfilename('fullpath');
[folder, name] = fileparts(thisFile);
cd(folder);

addpath('../mclasses');
addpath('../mfunctions');
addpath('../mex');
addpath('../mex/maxflow');

% only for development
%makeMex;



config = CellXConfiguration();

config.setMembraneIntensityProfile([0.6,0.4,0.2,0.1,0.1,0.2,0.4,0.7,0.8,0.9,0.8,0.6,0.5,0.4]);
config.setMembraneLocation(6);
config.setMembraneWidth(3);
config.setSeedRadiusLimit([10 27]);
config.setMaximumCellLength(120);
config.setHoughTransformOnCLAHEImage(0);
config.setSeedSensitivity(0.1);
%config.setCropRegion(200,200,300,300);102:[675 126 240 240]
config.setCropRegion(691, 310, 240, 240);
config.setDebugLevel(2);
config.check();


fs = CellXFileSet(1, '../data/img1.tif');

cellXSegmenter = CellXSegmenter(config, fs);

dbstop if error;

cellXSegmenter.run();

skipReasons = {cellXSegmenter.seeds.skipReason};
li = ~ strcmp(skipReasons, '');

for i=find(li)   
    fprintf('Filtered seed %d because of\n  %s\n\n', i, skipReasons{i});
end
fprintf('Filtered %d seeds\n', sum(li));




