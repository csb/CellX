clear all;

thisFile = mfilename('fullpath');
[folder, name] = fileparts(thisFile);
cd(folder);

addpath('../mclasses');
addpath('../mfunctions');
addpath('../mex');
addpath('../mex/maxflow');

tmpRS = [ -0.2202   -0.3452   -0.4702   -0.4702   -0.3952   -0.1327...
        0.1048    0.3048  0.5298    0.5298    0.3798    0.1548    0.0298];

    
% define midpoint
tempMidpoint = 5;    

% define a single Ray intensity profile    
tmpRay = [zeros(1,25) tmpRS zeros(1,25)];

conv = testRay(tmpRay, tmpRS, 5);

[v idx] = max(conv);

fprintf('Maximum at %d\n', idx);

if(idx~=30)
    error('Expected maximum at 30');
end

%plot(conv);

tmpRay = [tmpRS zeros(1,25)];

conv = testRay(tmpRay, tmpRS, 5);

[v idx] = max(conv);

fprintf('Maximum at %d\n', idx);

if(idx~=5)
    error('Expected maximum at 5');
end


plot(conv);

tmpRay = [zeros(1,25) tmpRS];

conv = testRay(tmpRay, tmpRS, 5);

[v idx] = max(conv);

fprintf('Maximum at %d\n', idx);

if(idx~=30)
    error('Expected maximum at 30');
end


%plot(conv);




