clear; clc; close;
dbstop if error

%-----required paths, etc
thisFile = mfilename('fullpath');
[folder, name] = fileparts(thisFile);
cd(folder);
addpath('../mclasses');
addpath('../mfunctions');
addpath('../mex');
addpath('../mex/maxflow');
addpath('../YouScopeInterface');


cnt=0;
for nrf = 2:5
    cnt=cnt+1;
    
    % --------START TEMP -----------
   
    % THE FOLLOWING PROCESS SHOULD BE DONE
    % IN YOUSCOPE. CHECK THE METHODS THAT ARE PROCESSING
    % THE INPUT IMAGE AND ADJUST ACCORDINGLY!!!
    
    % take the configuration file
    configFileName = '../data/endocytosis/configEndocytosisB.xml';
    config = CellXConfiguration.readXML(configFileName);
    config.check();
    % take current segmentation image
    currentImageFileName = ['../data/endocytosis/BFout_position010100_time000' num2str(nrf) '.tif'];
    % read, check, etc
    segmImage = CellXImageIO.loadToGrayScaleImage(currentImageFileName,config.cropRegionBoundary);
    % pass current fluo information: images, tags, flat-field paths
    fluoInitialImages={};
    fluoTags ={};
    flatFieldFileNames= {};
    if 1 % optional steps
        currentIntensityFileName1 = ['../data/endocytosis/gre_position010100_time000' num2str(nrf) '.tif'];
        currentIntensityFileName2 = ['../data/endocytosis/gre_position010100_time000' num2str(nrf) '.tif'];
        fluoInitialImages{1} = CellXImageIO.loadToGrayScaleImage(currentIntensityFileName1,config.cropRegionBoundary);
        fluoInitialImages{2} = CellXImageIO.loadToGrayScaleImage(currentIntensityFileName2,config.cropRegionBoundary);
        fluoTags = {'gre1','gre2'};
        flatFieldFileNames{1} = '../data/endocytosis/ffgre_position010100_time0002.tif';
        flatFieldFileNames{2} = '../data/endocytosis/ffgre_position010100_time0002.tif';
    end
    % --------END TEMP---------------
    
    
    % run the cellXYouScopeInterfacer 
    if cnt==1
        % initializations
        previousSegmentedCells=[];
        previousSegmentationMask=[];
        previousResult = [];
        % run first time-point analysis
        cellXYouScopeInterfacer = CellXYouScopeInterface(cnt, configFileName,segmImage,...
            fluoTags,flatFieldFileNames,fluoInitialImages,...
            previousSegmentedCells,previousSegmentationMask,...
            previousResult);
        cellXYouScopeInterfacer.run;
    else
        % pass information from previous frame/run
        previousSegmentedCells = cellXYouScopeInterfacer.currentSegmentedCells ;
        previousSegmentationMask = cellXYouScopeInterfacer.currentSegmentationMask;
        previousResult = cellXYouScopeInterfacer.currentResult;
        % run current analysis
        cellXYouScopeInterfacer = CellXYouScopeInterface(cnt, configFileName,segmImage,...
            fluoTags,flatFieldFileNames,fluoInitialImages,...
            previousSegmentedCells,previousSegmentationMask,...
            previousResult);
        cellXYouScopeInterfacer.run;
    end
    
    % --------START TEMP -----------
    % fluo-data acquition
    % we save the quantity  gre1.cell.total/cell.area
    % and the tracking indices
    if cnt==1
       fluodata =[]; 
    end
    trackdata(1:numel(cellXYouScopeInterfacer.currentSegmentedCells),cnt) = cellXYouScopeInterfacer.currentResult.data(:,end);

    fluodata(cellXYouScopeInterfacer.currentResult.data(:,end),cnt) =...
          cellXYouScopeInterfacer.currentResult.data(:,17)./cellXYouScopeInterfacer.currentResult.data(:,8);
     % --------END TEMP -----------
    
end

% --------START TEMP -----------
% plot only the full tracks
logIndx = zeros(size(fluodata,1),1);
for nrt = 1:size(fluodata,1);
    if ~any(fluodata(nrt,:)==0)
       logIndx(nrt,1)=1; 
    end
end
figure;
plot(fluodata(logical(logIndx),:)','.-');
% --------END TEMP -----------

