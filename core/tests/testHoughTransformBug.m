clear; clc; close;

thisFile = mfilename('fullpath');
[folder, name] = fileparts(thisFile);
cd(folder);

addpath('../mclasses');
addpath('../mfunctions');
addpath('../mex');
addpath('../mex/maxflow');

% only for development
%makeMex;
   
config = CellXConfiguration();

fs = CellXFileSet(1,'../data/OOF_position000000_time0053.tif');


config.setMembraneIntensityProfile([ 0.66,0.43,0.34,0.18,0.05,0.17,0.36,0.64,0.8,0.9,0.85,0.7,0.55,0.41]);
config.setMembraneLocation(7);
config.setMembraneWidth(3);
config.setSeedRadiusLimit([10 22]);
config.setMaximumCellLength(60);
config.setHoughTransformOnCLAHEImage(0);
config.setGraphCutOnCLAHEImage(1)
config.setSeedSensitivity(0.2);
%config.setCropRegion(1100, 150, 150, 150);
config.setDebugLevel(1);
config.check();

cellXSegmenter = CellXSegmenter(config, fs);

cellXSegmenter.run();

%cellXSegmenter.createResultImage(imgFilename)