clear; clc; close;


thisFile = mfilename('fullpath');
[folder, name] = fileparts(thisFile);
cd(folder);

addpath('../mclasses');
addpath('../mfunctions');
addpath('../mex');
addpath('../mex/maxflow');

config = CellXConfiguration();


%profile = [0 0 0.3 0.3 0.3 0.6 0.6 0.6 0.9 0.9 0.9 -0.5 -0.5 -0.5 0 0 ];
profile = [0 0 0.9 0.85 0.8 0.75 0.7 0.65 0.6 0.5 0.4 0.3 0.2 0.1 0 0 ];
config.setSeedRadiusLimit([30 40]);
config.setMembraneIntensityProfile(profile);
config.setMembraneLocation(2);
config.setMembraneWidth(5);
config.setMaximumCellLength(110);
config.setDebugLevel(6);
config.setSeedSensitivity(0.001);


img = 0.1* (rand(200,200)-0.5);


centers = [70 80; 75 120; 130 80];
radii = [42 45 50];

n = size(centers,1);

for i=n:-1:1

    for k = 1:12
        img = midpointcircle(img, radii(i)+(k-7), centers(i,1), centers(i,2), profile(k+2) );
    end
end


gauf = fspecial('gaussian', 1, 1);
blur = imfilter(img, gauf, 'conv');
minimumBitValue = min(blur(:));
maximumBitValue = max(blur(:)); 
variableBitRange = maximumBitValue - minimumBitValue;          

img = ((blur-minimumBitValue) / variableBitRange);    

%imagesc(img);


fileSet = CellXFileSet(2, '../data/results');
seg = CellXSegmenter(config, fileSet);
seg.run();

