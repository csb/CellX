clear; clc; close;

thisFile = mfilename('fullpath');
[folder, name] = fileparts(thisFile);
cd(folder);

addpath('../mclasses');
addpath('../mfunctions');
addpath('../mex');
addpath('../mex/maxflow');

config = CellXConfiguration.fromXML('../data/img1config.xml');
config.setCropRegion(300,300,200,200);
fileSet = CellXFileSet(1, '../data/img1.tif');



seg = CellXSegmenterFig(config, fileSet);

seg.run();

