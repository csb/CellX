clear; clc; close;


thisFile = mfilename('fullpath');
[folder, name] = fileparts(thisFile);
cd(folder);

addpath('../mclasses');
addpath('../mfunctions');
addpath('../mex');
addpath('../mex/maxflow');


config = CellXConfiguration.fromXML('../data/img1config.xml');
config.setHoughTransformOnCLAHEImage(1);

config.check();

config.setCropRegion(500, 100, 100, 100);
config.setDebugLevel(3);

fileSet = CellXFileSet(1, '../data/img1.tif');

cellXSegmenter = CellXSegmenter(config, fileSet);


%
% to be checked: seed 2 before refinement
%


cellXSegmenter.run();