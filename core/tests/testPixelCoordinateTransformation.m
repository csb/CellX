clear; clc; close;


thisFile = mfilename('fullpath');
[folder, name] = fileparts(thisFile);
cd(folder);

addpath('../mclasses');
addpath('../mfunctions');
addpath('../mex');
addpath('../mex/maxflow');

% only for development
%makeMex;



config = CellXConfiguration();

config.setMembraneIntensityProfile([0.6,0.4,0.2,0.1,0.1,0.2,0.4,0.7,0.8,0.9,0.8,0.6,0.5,0.4]);
config.setMembraneLocation(6);
config.setMembraneWidth(3);
config.setSeedRadiusLimit([10 27]);
config.setMaximumCellLength(120);
config.setHoughTransformOnCLAHEImage(1);
config.setSeedSensitivity(0.05);
config.setCropRegion(200,200,300,300);
config.setDebugLevel(0);
config.check();



fileSet = CellXFileSet(1, '../data/img1.tif');
cellXSegmenter = CellXSegmenter(config, fileSet);

%dbstop if error;

cellXSegmenter.run();

s = cellXSegmenter.seeds(1);



% crop = imcrop(cellXSegmenter.imageExtended, s.cropRegion);
% 
% crop(s.perimeterPixelListLindx) = 1;
% imshow(crop);
% 
% 
% imge = cellXSegmenter.image;
% [x y] = s.getInputImagePixelCoordinates(s.perimeterPixelListLindx);
% ind = sub2ind(size(imge), x,y);
% imge(ind) = 1;
% figure;
% imshow(imge);


rgb = cellXSegmenter.createResultImage('results.png');



