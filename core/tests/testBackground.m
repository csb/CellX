clear; clc; close;


thisFile = mfilename('fullpath');
[folder, name] = fileparts(thisFile);
cd(folder);

addpath('../mclasses');
addpath('../mfunctions');
addpath('../mex');
addpath('../mex/maxflow');


config = CellXConfiguration.fromXML('../data/img1config.xml');

config.setCropRegion(1,1,300,300);
fileSet = CellXFileSet(1, '../data/img1.tif');
seg = CellXSegmenter(config, fileSet);

seg.run();

