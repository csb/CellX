clear; clc; close;


thisFile = mfilename('fullpath');
[folder, name] = fileparts(thisFile);
cd(folder);
addpath('../mscripts');
addpath('../mclasses');
addpath('../mfunctions');
addpath('../mex');
addpath('../mex/maxflow');


% infile = '../data/IntensityExtraction/BFout_position010100_time0001.tif';
% configfile = '../data/IntensityExtraction/configIntensityExtractionCrop.xml';
% fluofile   = '../data/IntensityExtraction/gre_position010100_time0001.tif';
% flatfieldfile = '../data/IntensityExtraction/ffgre_position010100_time0001.tif'; 
% 
% 
% segmentationControlImage = '/tmp/segControl.png';
% intensityControlImage = '/tmp/intControl.png';
% 
% %(infile, configfile, controlImg, mode, varargin)
% calibrateCellX( ...
%     infile, ...
%     configfile, ...
%     segmentationControlImage, ...
%     '2');%, fluofile, intensityControlImage, flatfieldfile);


%config = '../data/endocytosis/configEndocytosis.xml';

 [~, user] = system('echo $USER')
 
 user = strtrim(user);
 
 if( strcmp(user, 'cmayer') )
     config = '../data/endocytosis/configEndocytosisTwoCellsCrop.xml';
     fileSeries = '../data/endocytosis/fileSeriesNewFormat.xml';
     resultsDir = '/Users/cmayer/results/';
 else
     config = '../data/endocytosis/configEndocytosisTwoCellsCrop.xml';
     fileSeries = '../data/endocytosis/fileSeriesLocal.xml';
     resultsDir = '/home/sotirisd/Desktop/PROJECTS/CellX/core/data/endocytosis/results';
 end


%batchCellX(config, '-s', fileSeries);


bf ='/home/cmayer/ethz/develop/matlab/CellX/core/data/endocytosis/BFout_position010100_time0002.tif';
fluo='/home/cmayer/ethz/develop/matlab/CellX/core/data/endocytosis/gre_position010100_time0002.tif';
ff ='/home/cmayer/ethz/develop/matlab/CellX/core/data/endocytosis/ffgre_position010100_time0002.tif';


batchCellX(config, '-m', 'files', ...
    '-i', 7, ...
    '-r', '/Users/cmayer/results/', ...
    '-b',  bf, ...
    '-ff', ff, ...
    '-f', fluo, ...
    '-f', fluo ...
    );













