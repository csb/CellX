clear; clc; close;

thisFile = mfilename('fullpath');
[folder, name] = fileparts(thisFile);
cd(folder);

addpath('../mclasses');
addpath('../mfunctions');
addpath('../mex');
addpath('../mex/maxflow');

% only for development
%makeMex;

ccase=2;

if ccase==2
    config = CellXConfiguration();
    
    imgFilename = '../data/Figure1/Trans_020011.tif';
    fileSet = CellXFileSet(2, imgFilename);
    
    config.setMembraneIntensityProfile([0.3,0.2,0.1,0.1,0.16,0.37,0.56,0.72,0.90,0.90,0.78,0.6,0.5]);
    config.setMembraneLocation(6);
    config.setMembraneWidth(5);
    config.setSeedRadiusLimit([17 27]);
    config.setMaximumCellLength(100);
    config.setHoughTransformOnCLAHEImage(0);
    config.setGraphCutOnCLAHEImage(0)
    config.setSeedSensitivity(0.25);
    config.setCropRegion(1100, 150, 150, 150);
    config.setDebugLevel(2);
    config.check();
        
elseif ccase==3
    
config = CellXConfiguration();

imgFilename = '../data/img1.tif';
fileSet = CellXFileSet(2, imgFilename);
    
config.setMembraneIntensityProfile([0.6,0.4,0.2,0.1,0.1,0.2,0.4,0.7,0.8,0.9,0.8,0.6,0.5,0.4]);
config.setMembraneLocation(8);
config.setMembraneWidth(3);
config.setSeedRadiusLimit([10 27]);
config.setMaximumCellLength(120);
config.setHoughTransformOnCLAHEImage(1);
config.setSeedSensitivity(0.05);
%config.setCropRegion(500,330,150,150);
config.setCropRegion(650,250,180,130);
config.setDebugLevel(2);
config.check();

end


cellXSegmenter = CellXSegmenter(config, fileSet);

cellXSegmenter.run();


%cellXSegmenter.createResultImage(imgFilename)

