function [BGInfo, BGInfoLow, BGInfoMiddle, BGInfoHigh] = BackgroundInfo(filename, cellstats1,NofGauss,FigNum,FrameID)

if iscell(filename)
    %produce the figure
    locF3 = '../PaperFigure3/' ;
    f3D1 = figure;
    axes1=axes('Parent',f3D1,'FontSize',16);
    hold(axes1,'all');
    for nc=1:length(filename)
        iR = filename{nc};
        cellstats = cellstats1{nc};
        [muR,vR,pR,BackIntMat,IntBins,prb,h]=EMSegAlt(iR,NofGauss,FigNum,cellstats,FrameID);
        
        SumProb = sum(prb,2);
        %Nlowprob = find(SumProb>1e-3*max(SumProb));
        Nlowprob = find(SumProb>min(SumProb));
        hf = h(Nlowprob,1);
        prbf = prb(Nlowprob,:);
        IntBinsf =  IntBins(Nlowprob,1);
        if nc==1
            plot(IntBinsf,hf,'b','Parent',axes1,'LineWidth',2);
            hold(axes1,'all');
            %            plot(IntBinsf,prbf,'r--','Parent',axes1,'LineWidth',2)
            %            hold(axes1,'all');
            plot(IntBinsf,SumProb( Nlowprob,1),'r','Parent',axes1,'LineWidth',2)
            hold(axes1,'all');
        else
            plot(IntBinsf,hf,'g','Parent',axes1,'LineWidth',2);
            hold(axes1,'all');
            %            plot(IntBinsf,prbf,'g--','Parent',axes1,'LineWidth',2)
            %            hold(axes1,'all');
            plot(IntBinsf,SumProb( Nlowprob,1),'k','Parent',axes1,'LineWidth',2)
            hold(axes1,'all');
        end
    end
    
    ylabel('Intensity Frequency','FontSize',18);
    fnc = [locF3 'JointCellBackgrdHist' FrameID];
    print(f3D1,'-depsc','-r1200',fnc);
    saveas(f3D1,fnc,'fig');
    
    BGInfo=[];
    
else
    if ischar(filename)
        iR=imread(filename);
        iR=double(iR);
    else
        iR = filename;
    end
    
    % find the background Information from the Rhod image
    [muR,vR,pR,BackIntMat]=EMSegAlt(iR,NofGauss,FigNum,cellstats1,FrameID);
    Nmaxp = find(pR==max(pR));
    BGmeanR = muR(Nmaxp);
    BGdevR = sqrt(vR(Nmaxp));
    BGInfo = {BGmeanR ,BGdevR, BackIntMat};
    
    Nmlow = find(muR==min(muR));
    BGmeanRLow = muR(Nmlow);
    BGdevRLow = sqrt(vR(Nmlow));
    BGInfoLow = {BGmeanRLow  ,BGdevRLow};
    
    NmHigh = find(muR==max(muR));
    BGmeanRHigh = muR(NmHigh);
    BGdevRHigh = sqrt(vR(NmHigh));
    BGInfoHigh = {BGmeanRHigh  ,BGdevRHigh};
    
    NmMiddle = find(muR<max(muR) & muR>min(muR));
    BGmeanRMiddle  = muR(NmMiddle );
    BGdevRMiddle  = sqrt(vR(NmMiddle ));
    BGInfoMiddle  = {BGmeanRMiddle   ,BGdevRMiddle };
    
end


function [mu,v,p,BackIntMat,IntBins,prb,h]=EMSegAlt(iima,k,VisOption,cellstats,FrameID)

% check image
iima=double(iima);
imcopy=iima;           % make a copy
%ima2 = medfilt2(imcopy, [3 3]); % take out salt and pepper noise

% exclude pixels that belong to cells
%take the cell pixel list
allcellpixellist = [];
for nrcells = 1:length(cellstats)
    curcellpixellist = cellstats(nrcells).PixelList;
    allcellpixellist = [allcellpixellist;curcellpixellist];
end
% take the whole image pixel list
Xsize = size(imcopy,2);%number of columns
Ysize = size(imcopy,1);% number of rows
ImMat = zeros(Ysize*Xsize,3);
ppointer = 0;
for nrpix = 1:Xsize
    for nrpiy =1:Ysize
        ppointer = ppointer+1;
        ImMat(ppointer,1) = nrpix;
        ImMat(ppointer,2) = nrpiy;
        ImMat(ppointer,3) = imcopy(nrpiy,nrpix);
    end
end
% remove for the image pixel list the cells
% find which are the common rows 
tf = ismember(ImMat(:,1:2), allcellpixellist, 'rows');
partIndx  = find(~tf);
% final pixel set
imaf= ImMat(partIndx ,3);
ima = medfilt1(imaf,3);
mi=min(ima);        % deal with negative
ima=ima-mi+1;

m=max(ima);
s=length(ima);

% create image histogram

h=histogram(ima);
x=find(h);
h=h(x);
x=x(:);h=h(:);

% initiate parameters

mu=(1:k)*m/(k+1);
v=ones(1,k)*m;
p=ones(1,k)*1/k;

% start process
sml = mean(diff(x))/1000;
wtag=1;
while(wtag==1)
    % Expectation
    prb = distribution(mu,v,p,x);
    scal = sum(prb,2)+eps;
    loglik=sum(h.*log(scal));
    
    %Maximization
    for j=1:k
        pp=h.*prb(:,j)./scal;
        p(j) = sum(pp);
        mu(j) = sum(x.*pp)/p(j);
        vr = (x-mu(j));
        v(j)=sum(vr.*vr.*pp)/p(j)+sml;
    end
    p = p + 1e-3;
    p = p/sum(p);
    
    % Exit condition
    prb = distribution(mu,v,p,x);
    scal = sum(prb,2)+eps;
    nloglik=sum(h.*log(scal));
    if isnan(nloglik)
        fprintf('IMAGE PROBLEM \n')
        break;
    end;
    if((nloglik-loglik)<0.0001)
        % find which gausian belongs to background
        bckdistrind = find(p==max(p));
        % reconstruct the intensity histogram
        NrSamplesForEveryBin = round(prb(:,bckdistrind)*s);
        BackIntMat = [];
        for nrbins = 1:length(NrSamplesForEveryBin)
            curbin = [];
            if NrSamplesForEveryBin(nrbins)~=0
             curbin = x(nrbins)*ones(NrSamplesForEveryBin(nrbins),1);
             BackIntMat =[BackIntMat ; curbin];
            end
        end
        break;
    end
end
IntBins = x+mi-1;
if VisOption==3
    % calculations for histogram

    SumProb = sum(prb,2);
    Nlowprob = find(SumProb>1e-2*max(SumProb));
    hf = h(Nlowprob,1);
    prbf = prb(Nlowprob,:);
    IntBinsf =  IntBins(Nlowprob,1);
    
    %produce the figure
    locF3 = '../PaperFigure3/' ; 
    f3D = figure;
    axes1=axes('Parent',f3D,'FontSize',16);
    hold(axes1,'all');
    %plot([1:110],imrayprofilef1(1:110),'Parent',axes1,'LineWidth',3);
    ylabel('Intensity Frequency','FontSize',18);
    plot(IntBinsf,hf,'Parent',axes1,'LineWidth',1.5);
    hold(axes1,'all');
    plot(IntBinsf,prbf,'g--','Parent',axes1,'LineWidth',2)
    hold(axes1,'all');
    plot(IntBinsf,SumProb( Nlowprob,1),'r','Parent',axes1,'LineWidth',3)
    fnc = [locF3 'BackgrdHist' FrameID];
    print(f3D,'-depsc','-r1200',fnc);
    saveas(f3D,fnc,'fig');
end
if VisOption==32
    % calculations for histogram

    SumProb = sum(prb,2);
    Nlowprob = find(SumProb>1e-2*max(SumProb));
    hf = h(Nlowprob,1);
    prbf = prb(Nlowprob,:);
    IntBinsf =  IntBins(Nlowprob,1);
    
    %produce the figure
    locF3 = '../PaperFigure3/' ; 
    f3F = figure;
    axes1=axes('Parent',f3F,'FontSize',16);
    hold(axes1,'all');
    %plot([1:110],imrayprofilef1(1:110),'Parent',axes1,'LineWidth',3);
    ylabel('Intensity Frequency','FontSize',18);
    plot(IntBinsf,hf,'Parent',axes1,'LineWidth',1.5);
    hold(axes1,'all');
    plot(IntBinsf,prbf,'g--','Parent',axes1,'LineWidth',2)
    hold(axes1,'all');
    plot(IntBinsf,SumProb( Nlowprob,1),'r','Parent',axes1,'LineWidth',3)
    fnc = [locF3 'CellIntensHist' FrameID];
    print(f3F,'-depsc','-r1200',fnc);
    saveas(f3F,fnc,'fig');
end

% calculate mask
mu=mu+mi-1;   % recover real range


% s=size(imcopy);
% mask=zeros(s);
% for i=1:s(1),
% for j=1:s(2),
%   for n=1:k
%     c(n)=distribution(mu(n),v(n),p(n),imcopy(i,j));
%   end
%   a=find(c==max(c));
%   mask(i,j)=a(1);
% end
% end

function y=distribution(m,v,g,x)
x=x(:);
m=m(:);
v=v(:);
g=g(:);
for i=1:size(m,1)
    d = x-m(i);
    amp = g(i)/sqrt(2*pi*v(i));
    y(:,i) = amp*exp(-0.5 * (d.*d)/v(i));
end


function[h]=histogram(datos)
datos=datos(:);
ind=find(isnan(datos)==1);
datos(ind)=0;
ind=find(isinf(datos)==1);
datos(ind)=0;
tam=length(datos);
m=ceil(max(datos))+1;
h=zeros(1,m);
for i=1:tam,
    f=floor(datos(i));
    if(f>0 & f<(m-1))
        a2=datos(i)-f;
        a1=1-a2;
        h(f)  =h(f)  + a1;
        h(f+1)=h(f+1)+ a2;
    end;
end;

h=conv(h,[1,2,3,2,1]);
h=h(3:(length(h)-2));
h=h/sum(h);
