clear; clc; close;


thisFile = mfilename('fullpath');
[folder, name] = fileparts(thisFile);
cd(folder);

addpath('../mclasses');
addpath('../mfunctions');
addpath('../mex');
addpath('../mex/maxflow');

config = CellXConfiguration();

config.setMembraneIntensityProfile([0,0,1,0,0]);
config.setMembraneLocation(3);
config.setMembraneWidth(2);
config.setMaximumCellLength(60);
config.setDebugLevel(0);

radii = [10 20 30 40 50];

for radius=radii

    fprintf('Cell radius %d\n', radius);
    img = zeros(121,121);

    img = midpointcircle(img, radius, 60, 60, 1);
 
    [grdX grdY] = gradient(img);
    gradientPhaseImage = atan(abs(grdX./grdY));
    
    seed = CellXSeed(60,60,radius);
    seed.cropCenterX = 60;
    seed.cropCenterY = 60;
    
    md   = CellXMembraneDetector(config);
    
    md.run(seed, img, gradientPhaseImage);
    
    expectedVolume =4/3*pi*radius^3;
    
    fprintf('Volume expected         : %12.4f   estimated: %12.4f  error: %+4.2f\n', expectedVolume, seed.cellVolume , (expectedVolume-seed.cellVolume)/expectedVolume);
    
    cytRadius = radius-config.membraneWidth;
    
    expectedCytVolume = 4/3*pi*(cytRadius)^3;
    
    fprintf('Cytosol volume expected : %12.4f   estimated: %12.4f  error: %+4.4f\n', expectedCytVolume, seed.cytosolVolume , (expectedCytVolume-seed.cytosolVolume)/expectedCytVolume);

end



