
clear; clc;


thisFile = mfilename('fullpath');
[folder, name] = fileparts(thisFile);
cd(folder);

addpath('../mclasses');
addpath('../mfunctions');
addpath('../mex');
addpath('../mex/maxflow');


s = CellXSeed(1,2,3);


gre1 = CellXFluoFeatures('gre1');

gre2 = CellXFluoFeatures('gre2');

s.addFluoFeatures(gre1);

s.addFluoFeatures(gre2);

filename = 'seedAndFluo.mat';
save(filename, 's');


clear s;


load(filename);
s.fluoFeatureList(1)
s.fluoFeatureList(2)


