
clear;


addpath('../mclasses');



fSet = CellXFileSet(1, 'img.tif'); 


h = CellXFileHandler.fromXML('../data/endocytosis/fileSeries.xml', 'resultDir');

for i=1:numel(h.fileSets)
    h.fileSets(i)
end



fprintf('\n\nNEW FORMAT\n\n');


h = CellXFileHandler.fromCellXFilesXML('../data/endocytosis/fileSeriesNewFormat.xml',1);

for i=1:numel(h.fileSets)
    h.fileSets(i)
end

h