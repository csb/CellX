if 1
    
    clear; clc; close;
    dbstop if error
    
    thisFile = mfilename('fullpath');
    [folder, name] = fileparts(thisFile);
    cd(folder);
    
    addpath('../mclasses');
    addpath('../mfunctions');
    addpath('../mex');
    addpath('../mex/maxflow');
    

    config = CellXConfiguration();
    
    config.setMembraneIntensityProfile([ 0 0 1 1]);
    config.setMembraneLocation(2);
    config.setMembraneWidth(2);
    config.setSeedRadiusLimit([10 50]);
    config.setMaximumCellLength(100);
    config.setHoughTransformOnCLAHEImage(0);
    config.setGraphCutOnCLAHEImage(0)
    config.setSeedSensitivity(0.2);
    config.setFluoAlignPixelMove(0);
    config.setDebugLevel(0);
    config.check();
    
    NrFrames = 10;
    
    %AllFilesNames = cell(NrFrames,3);
    
    k=1;
    
    filename  = ['./testTracking/img' num2str(k) '.tiff'];
   
    fileHandler = CellXFileHandler('dir');
    
    fSet = CellXFileSet(1, filename);
    
    fSet.addFluoImageTag(...
        ['./testTracking/gre_img' num2str(k) '.tiff'], ...
        'gre1','');

    fileHandler.addFileSet(fSet);
     
    fSet = fileHandler.fileSets(k);
    
    
    cellXSegmenter = CellXSegmenter(config, fSet);
    
    cellXSegmenter.run();
    
    segmentedCells = cellXSegmenter.getDetectedCells();
    
    cellXIntensityExtractor = CellXIntensityExtractor(config, fSet, segmentedCells);
    
    cellXIntensityExtractor.run();
            
    
else
    
    
    clear; clc; close;
    dbstop if error
    
    thisFile = mfilename('fullpath');
    [folder, name] = fileparts(thisFile);
    cd(folder);
    
    addpath('../mclasses');
    addpath('../mfunctions');
    addpath('../mex');
    addpath('../mex/maxflow');
    
    % only for development
    %makeMex;
    
    config = CellXConfiguration();
    
    config.setMembraneIntensityProfile([ 0.5092  , 0.3592  ,  0.2451  ,  0.1556  ,  0.0938  ,  0.1000   , 0.1792  , ...
        0.3292 , 0.6176 ,   0.6176  ,  0.7419   , 0.7696  ,  0.7710  ,  0.7215  ,...
        0.5267 , 0.3740   , 0.3072  ]);
    config.setMembraneLocation(5);
    config.setMembraneWidth(8);
    config.setSeedRadiusLimit([20 40]);
    config.setMaximumCellLength(100);
    config.setHoughTransformOnCLAHEImage(0);
    config.setGraphCutOnCLAHEImage(1)
    config.setSeedSensitivity(0.5);
    config.setCropRegion(610, 900, 270, 250);
    config.setDebugLevel(0);
    config.check();
    
    
    
    fileHandler = CellXFileHandler('dir');
    
    
    fs = CellXFileSet(1,'../data/IntensityExtraction/BFout_position010100_time0001.tif');
    fs.addFluoImageTag(...
        '../data/IntensityExtraction/gre_position010100_time0001.tif', ...
        'gre1', ...
        '../data/IntensityExtraction/ffgre_position010100_time0001.tif');
    fs.addFluoImageTag(...
        '../data/IntensityExtraction/gre_position010100_time0001.tif', ...
        'gre2', ...
        '../data/IntensityExtraction/ffgre_position010100_time0001.tif');
    
    fileHandler.addFileSet(fs);
    
    
    
    
    fs = CellXFileSet(2,'../data/IntensityExtraction/BFout_position010100_time0001.tif');
    fs.addFluoImageTag(...
        '../data/IntensityExtraction/gre_position010100_time0001.tif', ...
        'gre1');
    
    fileHandler.addFileSet(fs);
    
    fs = CellXFileSet(3,'../data/IntensityExtraction/BFout_position010100_time0001.tif');
    fileHandler.addFileSet(fs);
    
    fs = CellXFileSet(1,'../data/IntensityExtraction/BFout_position010100_time0001.tif');
    fs.addFluoImageTag(...
        '../data/IntensityExtraction/gre_position010100_time0001.tif', ...
        'gre1', ...
        '../data/IntensityExtraction/ffgre_position010100_time0001.tif');
    fileHandler.addFileSet(fs);
    
    
    for i=1:1%fileHandler.getNumberOfFileSets()
        
        fSet = fileHandler.fileSets(i);
        
        cellXSegmenter = CellXSegmenter(config, fSet);
        
        cellXSegmenter.run();
        
        segmentedCells = cellXSegmenter.getDetectedCells();
        
        cellXIntensityExtractor = CellXIntensityExtractor(config, fSet, segmentedCells);
        
        cellXIntensityExtractor.run();
        
    end
    
    
    
end

