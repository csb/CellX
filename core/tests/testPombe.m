clear; clc; close;


thisFile = mfilename('fullpath');
[folder, name] = fileparts(thisFile);
cd(folder);

addpath('../mclasses');
addpath('../mfunctions');
addpath('../mex');
addpath('../mex/maxflow');


config = CellXConfiguration();


config.setSeedRadiusLimit([17 27]);
config.setMaximumCellLength(130);
config.setMembraneIntensityProfile([0.37,0.22,0.1,0.1,0.16,0.37,0.56,0.72,0.90,0.90,0.78,0.6,0.5,0.4]);
config.setMembraneLocation(5);
config.setMembraneWidth(3);
config.setSeedSensitivity(0.3);
config.setDebugLevel(1);
config.setGraphCutOnCLAHEImage(1);
%config.setCropRegion(600, 500, 300, 300);
%config.setOverlapResolveThreshold(0.6);
%config.setMaximumMinorAxisLengthHoughRadiusRatio(3);

fileSet = CellXFileSet(1, '../data/img6.tif');
seg = CellXSegmenter(config, fileSet);

seg.run();
