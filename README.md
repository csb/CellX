Info
=====
CellX was developed at the CSB group [[1]], and consists of the CellX executable (this project) as well as the GUI. 
Details on the usage of CellX for segmentation are provided in [[2]], more information on the method can be found in [[3]].

## How to start
### Using the compiled software with the user interface:

1. Download the latest release for for your operating system

      ​	win: https://gitlab.com/csb.ethz/CellX/-/blob/master/releases/win_g2.12_c2.12.zip

      ​	osx: https://gitlab.com/csb.ethz/CellX/-/blob/master/releases/osx_g2.12_c2.12.zip

      ​	unix: https://gitlab.com/csb.ethz/CellX/-/blob/master/releases/unix_g2.12_c2.12.zip

2. Download and install the Matlab MCR  9.0.1 (Matlab 2016a) . The MCR can be downloaded   from here if not included in the zip file directly:
   https://ch.mathworks.com/de/products/compiler/matlab-runtime.html
   
3. Start the graphical user interface (GUI)  and set the path for the CellX executable and MCR (Windows sets the MCR automatically). Note: Java 1.8 or higher needs to be installed.

```
/CellX_g2.12/CellXGui.jar
```



![](static/img/openSettingsDlg.png)



![](static/img/settingsDlgWin.PNG)



![](static/img/settingsDlgOSX.jpg)

4. Open an image to segment, define the parameters and hit Test Seeding or Test Segmentation. For batch segmentation switch to the batch processing Tab and select all images to segment and quantify.


### From source:

1. Clone or download this repo and run the _runExamples.m_ file in CellX/example_singleImages/
2. (Optional) Download the graphical user interface:
   https://gitlab.com/csb.ethz/CellXGui/-/tree/master/dist

   
### Compile CellX

Make sure that you fullfill all prerequisites:

* XCode and Command Line Tools installed and licence accepted (OSX)
* MATLAB Application compiler toolbox
* MATLAB Imageing Processing toolbox
* Signal Processing toolbox

Unter `/core/deploy` run either `makeNewRelease.m` from the CLI or open the projects depending on your platform.


License
-------
CellX is provided under the [simplified BSD license](LICENSE.txt).

Authors
-------
    Sotiris Dimopoulos <sotiris.dimopoulos@gmail.com>
    Christian Mayer <chrixian.mayer@gmail.com>

Current Maintainer:
    Andreas Cuny <andreas.cuny@bsse.ethz.ch>

References
----------

[1]: http://www.csb.ethz.ch/tools/software/cellx.html
1. [CellX documentation on the CSB group website][1]
[2]: http://doi.org/10.1002/0471142727.mb1422s101
2. Mayer C, Dimopoulos S, Rudolf F, Stelling J (2013)  
Using CellX to quantify intracellular events  
[Curr Protoc Mol Biol Chapter 14: Unit 14 22.][2]
[3]: http://doi.org/10.1093/bioinformatics/btu302
3. Dimopoulos S, Mayer CE, Rudolf F, Stelling J (2014)  
Accurate cell segmentation in microscopy images using membrane patterns.  
[Bioinformatics 30: 2644-2651.][3]